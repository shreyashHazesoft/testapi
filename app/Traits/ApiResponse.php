<?php
namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


trait ApiResponse
{
    // protected function response(
    //     ?string $message = null,
    //     int $responseCode = Response::HTTP_OK,
    //     mixed $payload = null,
    // ): JsonResponse {
    //     $response = [
    //         "message" => json_decode($message) ?? $message,
    //         "data" => is_object($payload)
    //             ? $payload->response()->getData(true)
    //             : $payload,
    //     ];

        // if ($payload === null) {
        //     unset($response["data"]);
        // }

        // if ($response["message"] == null) {
        //     unset($response["message"]);
        // }

    //     return response()->json($response, $responseCode);
    // }

    protected function successResponse(
        string $message = '',
        array|string|object $data = null,
        int $status_code = Response::HTTP_OK
    ): JsonResponse
    {
        $payload['status'] = 'success';

        if ($message) {
            $payload['message'] = $message;
        }

        if ($data) {
            $payload['payload'] = $data;
        }

        return response()->json($payload, $status_code);
    }

    protected function errorResponse(
        string $message = null,
        array|string|object $data = null,
        int $status_code = Response::HTTP_INTERNAL_SERVER_ERROR
    ): JsonResponse
    {
        $payload['status'] = 'error';

        if ($message) {
            $payload['message'] = $message;
        }

        if ($data) {
            $payload['payload'] = $data;
        }

        return response()->json($payload, $status_code);
    }
}

