<?php

namespace App\Auth\Grants;

use App\Models\User;
use App\Services\TOTPService;
use Illuminate\Support\Facades\Cache;

class OtpVerifier
{
    public TOTPService $totpService;

    public function __construct(TOTPService $totpService)
    {
        $this->totpService = $totpService;
    }
    public function verify(int $otp): object|bool
    {
        $hash  = Cache::get("hashKey");
        $user = User::where("hash_key", $hash)->first();
        $secret = $user->secret;
        $secretOtp = $this->totpService->generateOtp($secret);

        if ($otp == $secretOtp) {
            return $user;
        } else {
            return false;
        }
    }
}