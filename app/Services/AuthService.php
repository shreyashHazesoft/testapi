<?php

namespace App\Services;

use App\Jobs\LoginJob;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\AuthorizationServer;
use Psr\Http\Message\ServerRequestInterface;

class AuthService
{
    public ServerRequestInterface $serverRequest;
    public TokenRepository $token;
    public AuthorizationServer $server;

    public function __construct(
        ServerRequestInterface $serverRequest,
        TokenRepository $token,
        AuthorizationServer $server
    ) {
        $this->serverRequest = $serverRequest;
        $this->token = $token;
        $this->server = $server;
    }

    public function getAccessToken(object $request, object $user): array
    {
        $payload = [
            "grant_type" => "password",
            "client_id" => env("OAUTH_CLIENT_ID"),
            "client_secret" => env("OAUTH_CLIENT_SECRET"),
            "username" => $request->email,
            "password" => $request->password
        ];

        $accessToken = new AccessTokenController($this->server, $this->token);
        $result = $accessToken->issueToken($this->serverRequest->withParsedBody($payload));
        $response = json_decode($result->getOriginalContent(), true);
        unset($response["refresh_token"]);

        if ($response) {
            dispatch(new LoginJob($user));
        }

        return $response;
    }

    public function getAccessTokenFor2fa(object $request, object $user): array
    {
        $payload = [
            "grant_type" => "otp_grant",
            "client_id" => env("OAUTH_CLIENT_ID"),
            "client_secret" => env("OAUTH_CLIENT_SECRET"),
            "username" => $user->email,
            "password" => "",
            "otp" => $request->otp,
        ];
        $accessToken = new AccessTokenController($this->server, $this->token);
        $result = $accessToken->issueToken($this->serverRequest->withParsedBody($payload));
        $response = json_decode($result->getOriginalContent(), true);
        unset($response["refresh_token"]);

        if ($response) {
            dispatch(new LoginJob($user));
        }

        return $response;
    }

    public function generateHash(): string
    {
        $hashkey = Hash::make(rand(10000000, 99999999));
        Cache::put(['hashKey' => $hashkey], now()->addSecond(1600));
        return $hashkey;
    }

    public function savehash($user, $hash): bool
    {
        $user->hash_key = $hash;
        return $user->save();
    }

    public function generateToken(): string
    {
        $token = Str::random(50);
        return $token;
    }
}
