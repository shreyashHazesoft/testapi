<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use OTPHP\TOTP;
use ParagonIE\ConstantTime\Base32;

class TOTPService
{
    public function getUserSecret(object $user): string
    {
        if($user->secret) {
            return $user->secret;
        }

        $secret = trim(Base32::encodeUpper(random_bytes(20)), '=');
        $otp = TOTP::createFromSecret($secret);
        $secret = $otp->getSecret();
        $user->secret = $secret;
        $user->update(['secret'=> $secret]);

        return $secret;
    }

    public function generateOtp(string $secret): string
    {
        $timestamp = time();

        $otp = TOTP::create($secret);

        $code = $otp->at($timestamp);

        return $code;
    }
}
