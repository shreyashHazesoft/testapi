<?php

namespace App\Listeners;

use App\Events\LoginMailEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class LoginMailListner
{
    public function __construct()
    {
        //
    }

    public function handle(LoginMailEvent $event)
    {
        $user = User::find($event->userId)->toArray();

        Mail::send("email.loginMail", $user, function($message) use ($user) {
            $message->to($user['email']);
            $message->subject("Email about Login In the Debrath Foundation App");
        });
    }
}
