<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Services\AuthService;
use App\Services\TOTPService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;

class UserController extends BaseController
{
    use ApiResponse;

    public TOTPService $TOTPService;
    public AuthService $authService;

    public function __construct(TOTPService $TOTPService, AuthService $authService)
    {
        $this->TOTPService = $TOTPService;
        $this->authService = $authService;
    }

    public function login(Request $request): JsonResponse
    {
        try {
            $validData = $request->validate([
                "email" => "required|email",
                "password" => "required"
            ]);
            if (Auth::attempt($validData)) {
                $user = Auth::user();
                if ($user->is2fa) {
                    $secret = $this->TOTPService->getUserSecret($user);
                    $hash = $this->authService->generateHash();
                    $this->authService->savehash($user, $hash);
                    return $this->successResponse("now you will be redirected to verifyTOTP route", $hash, Response::HTTP_OK);

                } else {
                    $token = $this->authService->getAccessToken($request, $user);
                    return $this->successResponse("login successfull", $token, Response::HTTP_OK);
                }
            } else {
                throw new Exception("credential didnt match");
            }
        } catch (Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }
    }

    public function verifyTOTP(Request $request): JsonResponse
    {
        try {
            $request_totp = $request->otp;

            $hashKey = Cache::get('hashKey');
            $user = User::where('hash_key', $hashKey)->first();
            $secret= $user->secret;

            $totp = $this->TOTPService->generateOTP($secret);
            if ($request_totp == $totp) {
                $token =  $this->authService->getAccessTokenFor2fa($request, $user);
                return $this->successResponse("login successfull", $token, Response::HTTP_OK);
            } else {
                throw new Exception("otp didn't match");
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage());
        }
    }

    public function register(Request $request): JsonResponse
    {
        try {
            $validate = $request->validate([
                "name" => "required",
                "email" => "required|email",
                "password" => "required",
            ]);

            $validate['password'] = Hash::make($request->password);
            $user = User::create($validate);

            if (! $user) {
                throw new Exception("error");
            }

            return $this->successResponse("User Registeres successfully", Auth::user(), Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }
    }
}
