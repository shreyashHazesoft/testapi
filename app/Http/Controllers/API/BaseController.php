<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    use ApiResponse;

    protected $resources;

    public function __construct(object $resources)
    {
        $this->resources = $resources;
    }

    public function handleException(object $exception): JsonResponse
    {
        return $this->errorResponse(
            message: $exception->getMessage(),
            // responseCode: $exception->getStatusCode()
        );
    }
}

