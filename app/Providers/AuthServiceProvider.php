<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Auth\Grants\OtpVerifier;
use App\Auth\Grants\OtpGrant;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Bridge\UserRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\AuthorizationServer;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerPolicies();

        Passport::tokensExpireIn(now()->addSecond(3600));

        app(AuthorizationServer::class)->enableGrantType($this->makeOtpGrant(), Passport::tokensExpireIn());
    }


    protected function makeOtpGrant()
    {
        $grant = new OtpGrant(
            $this->app->make(UserRepository::class),
            $this->app->make(RefreshTokenRepository::class),
            $this->app->make(OtpVerifier::class)
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }
}
