<?php

namespace App\Repositories;

use App\Models\User;

class AuthRepository extends BaseRepository
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct($user);
        $this->rules = [
            "email" => "required|email",
            "password" => "required|min:6",
        ];
    }
}
