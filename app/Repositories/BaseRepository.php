<?php

namespace App\Repositories;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BaseRepository
{
    public array $rules = [];
    public array $messages = [];
    protected $model;

    public function __construct (object $model)
    {
        $this->model = $model;
    }

    public function validateData(
        object $request,
        array $rules = [],
        array $messages = []
    ): object | array {
        $data = $request->validate(
            array_merge($this->rules, $rules),
            array_merge($this->messages, $messages)
        );

        return $data;
    }

    public function login(array $data): array
    {
        try {
            $token = Auth::attempt($data);
            $user = Auth::user();
            if (!$user) {
                throw new Exception(__("auth.failed"), 404);
            }
        } catch (Exception $exception) {
            throw $exception;
        }

        return [$user, "token" => $token];
    }

    public function create(array $data, ?callable $callback = null): object
    {
        DB::beginTransaction();

        try {
            $rows = $this->model::create($data);
            if ($callback) {
                $callback($rows);
            }
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        DB::commit();
        return $rows;
    }
}
