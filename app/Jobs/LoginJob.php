<?php

namespace App\Jobs;

use App\Events\LoginMailEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LoginJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public function __construct(object $user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        if ($this->user) {
            event(new LoginMailEvent($this->user));
        }
    }
}
