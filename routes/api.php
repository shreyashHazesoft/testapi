<?php

use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post("login", [UserController::class, "login"]);
// Route::post("login", [UserController::class, "otpLogin"]);
Route::post("register", [UserController::class, "register"]);
Route::post("verifytotp", [UserController::class, "verifyTOTP"]);
